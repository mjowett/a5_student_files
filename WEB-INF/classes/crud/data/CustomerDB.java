package crud.data;

import java.sql.*;

import crud.business.Customer;

public class CustomerDB {

    public static int insert(Customer customer) {
        ConnectionPool pool = ConnectionPool.getInstance();
        Connection connection = pool.getConnection();
        PreparedStatement ps = null;
				//unfortunately, JDBC doesn't make it easy to use named parameters (:name) instead of indices (1, 2, 3, etc.).  :(
        String query
					= "INSERT INTO customer (cus_fname, cus_lname, cus_email) "
                + "VALUES (?, ?, ?)";
        try {
            ps = connection.prepareStatement(query);
            ps.setString(1, customer.getFname());
            ps.setString(2, customer.getLname());
            ps.setString(3, customer.getEmail());
						
            return ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
            return 0;
        } finally {
            DBUtil.closePreparedStatement(ps);
            pool.freeConnection(connection);
        }
    }
}
